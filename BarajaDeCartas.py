from Carta import Carta
from random import randint
class BarajaDeCartas:
    actual=0
    def __init__(self,baraja_size):
        self.baraja_size=baraja_size
        self.carta_actual=" "
        self.baraja=self.inicializar()

    def inicializar(self):
        baraja=[]
        numeros=["As","Dos","Tres","Cuatro","Cinco","Seis","Siete","Ocho","Nueve","Sota","Caballo","Rey"]
        tipo=["Oro","Bastos","Espadas","Copas"]
        i = 0
        x = 0
        for y in range (0,self.baraja_size):
            baraja.append(Carta(numeros[x], tipo[i]))
            if x==11 and tipo!=3:
                i=i+1
                x=0
            else:
                x=x+1
        return baraja

    def barajar(self):
        barajada=[]
        for i in range (0,self.baraja_size):
            num=self.baraja[randint(0,self.baraja_size-1-i)]
            barajada.append(num)
            self.baraja.remove(num)
        self.baraja = barajada
        self.carta_actual = self.baraja[0]


    def repartir(self):
        self.actual=self.actual+1
        self.carta_actual=self.baraja[self.actual]
        aux = self.carta_actual
        return aux
    def mostrarCarta(self):
            self.baraja[self.actual].toString()
